#!/usr/bin/env python3

from sys import argv

def exitMessage():
	print("Conflict-serializablity checker @Gabriel Gorski")
	print("Please input data separated by spaces like this:\n")
	print("r1x w2x r2y\n")
	print("Order of items represents the passage of time")
	print("Single item format: r|w (read/write) 1|2|3|.. (transaction number) a|b|c|.. (variable name)")
	exit(-1)

class Schedule:
	# Fields:
	# self.operations # type:list
	# self.conflictingOperations # type:list
	# self.isConflictSerializable # type:bool
	# self.transactionsWithCycles # type:list

	def __init__(self, operations):
		self.parse(operations)
		self.retrieveConflictingOperations()
		self.checkIfAcyclic()

	def parse(self, operations):
		self.operations = [(string[0], int(string[1]), string[2]) \
		if len(string) == 3 and string[0] in ('r', 'w') and string[1].isdecimal() and string[2].isalpha() \
		else (print("Wrong single argument"), exitMessage()) \
		for string in operations]

	def retrieveConflictingOperations(self):
		listWithRepetitions = []
		for i, operation1 in enumerate(self.operations):
			for j, operation2 in enumerate(self.operations):
				if operation1[1] != operation2[1] and \
				   operation1[2] == operation2[2] and \
				   'w' in (operation1[0], operation2[0]):
				   	listWithRepetitions.append((operation1, operation2)) \
				   	if i < j else listWithRepetitions.append((operation2, operation1))
		self.conflictingOperations = sorted(set(listWithRepetitions), key=listWithRepetitions.index)

	def checkIfAcyclic(self):
		self.isConflictSerializable = True
		setOfTransactionsWithCycles = set()
		for op1Before, op1After in self.conflictingOperations:
			for op2Before, op2After in self.conflictingOperations:
				if op1Before[1] == op2After[1] and \
				   op1After[1] == op2Before[1] and \
				   op1Before[2] == op2Before[2]:
					self.isConflictSerializable = False

					setOfTransactionsWithCycles.add((op1Before[1], op2Before[1])) \
					if op1Before[1] < op2Before[1] \
					else setOfTransactionsWithCycles.add((op2Before[1], op1Before[1]))
		self.transactionsWithCycles = sorted(setOfTransactionsWithCycles, key = lambda x : x[0])

	def printInfo(self):
		print("Operations:")
		for i, x in enumerate(self.operations):
			print(str(i) + ": " + x[0] + "|" + str(x[1]) + "|" + x[2])
		print("Conflicting operations:")
		for i, x in enumerate(self.conflictingOperations):
			print(str(i) + ": " + x[0][0] + "|" + str(x[0][1]) + "|" + x[0][2] + " and " + x[1][0] + "|" + str(x[1][1]) + "|" + x[1][2])
		if not self.isConflictSerializable:
			print("Precedence graph has got a cycle. Schedule is NOT conflict-serializable")
			print("Conflicting transactions:")
			for transactions in self.transactionsWithCycles:
				print("- " + str(transactions[0]) + ", " + str(transactions[1]))
		else:
			print("Precedence graph is acyclic. Schedule IS conflict-serializable")


if len(argv) == 1 or argv[1] == '--help':
	exitMessage()

Schedule(argv[1:]).printInfo()
