#!/usr/bin/env python3.6

from anytree import Node
from anytree import RenderTree
from anytree import ContRoundStyle


def exitMessage():
    print("Simple SQL Query to R.A. parser. @Gabriel Gorski")


operations = {"SELECT": "pi", "FROM": "x", "WHERE": "sigma"}


class SqlParser:
    def __init__(self, sqlQuery):
        self.sqlQuery = sqlQuery
        self.queriesForOperations = {}

        self.parse()

    def parse(self):
        self.sqlQuery = self.sqlQuery.split('\n')[1:-1]

        for operation in self.sqlQuery:
            operation = operation.replace(',', '').split()
            self.queriesForOperations[operations[operation[0]]] = operation[1:]

    def generateTree(self):
        projection = Node("projection: " + ", ".join(self.queriesForOperations["pi"][:]))
        selection = Node("sigma: " + " ".join(self.queriesForOperations["sigma"][:]), parent=projection)
        cartesianProduct = [Node(relationName + " x") for relationName in self.queriesForOperations["x"]]
        cartesianProduct[1].name = cartesianProduct[1].name[:-2]

        if len(cartesianProduct) > 2:
            cartesianProduct[0].parent = cartesianProduct[1].parent = cartesianProduct[2]
        else:
            cartesianProduct[0].parent = cartesianProduct[1].parent = selection
        cartesianProduct[-1].parent = selection

        for i, cartesianProductOperand in enumerate(cartesianProduct[2:-1]):
            cartesianProductOperand.parent = cartesianProduct[i+2+1]
        for pre, fill, node in RenderTree(projection):
            print('{0}{1}'.format(pre, node.name))


    def print(self):
        self.generateTree()

sqlQuery = \
'''
SELECT R.a, S.b, D.c
FROM R, S, E
WHERE R.a = S.b, R.b = S.c
'''

SqlParser(sqlQuery).print()
